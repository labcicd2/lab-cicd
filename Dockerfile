FROM openjdk:17-jdk-slim

LABEL maintainer = "Boubacar Siddy DIALLO boubasiddy00@gmail.com"

ADD target/lab-cicd-0.0.1.jar lab-cicd.jar

ENTRYPOINT ["java", "-jar", "lab-cicd.jar"]